__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'

"""
Script takes parameters and assemble a table from chopped JSON
column alongside of the user-selected columns.

User selects:
 - JSON COLUMN (dictionary)
 - ID column
 - Columns to bring from the original table as well

NO PANDAS HAVE BEEN KILLED DURING CREATION OF THIS SCRIPT
"""

# Import Libraries
import os
import sys
import json
import csv
import logging
from keboola import docker

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(filename='python_job.log',level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

## initialize application
cfg = docker.Config('/data/')

## access the supplied values
params = cfg.get_parameters()
keep_columns = cfg.get_parameters()["keep_columns"]
json_column = cfg.get_parameters()["json_column"]
id_column = cfg.get_parameters()["id_column"]
# in production, put this into parameter window:
"""
{
  "keep_columns": [],
  "json_column": "",
  "id_column": ""
}
"""

# Get proper list of tables
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))
out_tables = cfg.get_expected_output_tables()
logging.info("OUT tables mapped: "+str(out_tables))



def get_tables(in_tables, out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: '" + str(in_name)+"'")
    logging.info("Input table source: '" + str(in_destination)+"'")

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: '" + str(out_name)+"'")
    logging.info("Output table destination: '" + str(out_Destination)+"'")

    return in_name, out_name


def output_file(output_model, file_out):
    """
    Save output data as CSV (without pandas)
    """

    with open(file_out, 'w') as csvfile:
        file_writer = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in output_model:
            file_writer.writerow(row)
    logging.info("File produced.")


def input_file(file_in):
    """
    Read input data as CSV DataFrame (without pandas)
    """
    with open(file_in, 'r', encoding='utf-8') as csvfile:
        file_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        input_data = list(file_reader)
    logging.info("Input file read.")

    return input_data


def get_column_position(columns,col_name):
    """
    This function checks column name and find the position in the list
    """
    position = [i for i, x in enumerate(columns) if x == col_name]
    try:
        assert len(position) == 1
    except AssertionError:
        logging.error("Column "+col_name+" not found! Found: {valsum} columns".format(valsum=len(position)))
        sys.exit(1)

    position_str = int(position[0])
    logging.info("Column '"+col_name+"' found on position: "+str(position_str))

    return position_str


def create_table(model):
    """
    Create data model including headers:
     - Assemble columns
     - Assemble data part
     - Assemble header
     - Produce Final data
    """

    ## find relevant columns
    columns = list(model[0])
    logging.info("Columns available: "+str(columns))

    ## Get column indexes for addressing
    column_location = get_column_position(columns,json_column)
    id_col = get_column_position(columns,id_column)
    keep_values = []
    for col_keep in keep_columns:
        keep_values.append(get_column_position(columns,col_keep))
    #print(keep_values)

    ## Assembly the data part
    data = []
    model = model[1:]
    print("--------------------")
    for a in model:
        try:
            json_data = json.loads(a[column_location])
        except:
            logging.error("Could not load cell as JSON file. Truncated text? Exit.")
            exit(1)
        box_data = []

        # insert data (PK, json key, json value)
        for key,value in json_data.items():
            values = [a[id_col], key,value]

            ## insert keep columns values
            for ins in keep_values:
                values.insert(1, a[ins])
            data.append(values)
    #print(data)

    ## Assemble the header
    ## header is ID-KeepColumns-KEY-VALUE
    header = keep_columns[::-1]
    header.insert(0, id_column)
    header.append(json_column+"_key")
    header.append(json_column+"_value")
    data.insert(0, header)
    logging.info("Table assembled.")
    #print(data)

    return data


if __name__ == "__main__":
    """
    Run the whole script
    """

    # determine data destination
    file_in, file_out = get_tables(in_tables, out_tables)
    logging.debug("Mapped files. \n IN: {0} \n OUT: {1}".format(file_in, file_out))

    model = input_file(file_in)
    output_model = create_table(model)
    output_file(output_model, file_out)

    logging.info("Script finished.")
    print("Script finished.")
